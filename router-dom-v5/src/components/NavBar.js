import React, { Component } from "react";
import { Nav, Navbar, NavbarBrand, NavItem } from "reactstrap";
import { Link } from "react-router-dom";

class NavBar extends Component {
  state = { isOpen: false };
  toggleCollapse = () => {};

  render() {
    return (
      <Navbar color="dark" dark expand="lg">
        <NavbarBrand href="/">Binar</NavbarBrand>
        {/* <NavbarToggler onClick={this.toggleCollapse}>
          <Collapse isOpen={this.state.isOpen} navbar> */}
        <Nav className="mr-auto" navbar>
          <NavItem className="px-2">
            <Link to="/">Home</Link>
          </NavItem>
          <NavItem className="px-2">
            <Link to="/about">About</Link>
          </NavItem>
          <NavItem className="px-2">
            <Link to="/contact">Contact</Link>
          </NavItem>
          <NavItem className="px-2">
            <Link to="/login">Login</Link>
          </NavItem>
        </Nav>
        {/* </Collapse>
        </NavbarToggler> */}
      </Navbar>
    );
  }
}

export default NavBar;
