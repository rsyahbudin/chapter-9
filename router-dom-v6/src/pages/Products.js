import { Link } from "react-router-dom";

/**
 * Note:
 * <Link to="/products/p1" .... could also be written as
 * <Link to"p1" .... with React Router DOM v6
 */

const Products = () => {
  return (
    <section>
      <h1>The Products Page</h1>
      <ul>
        <li>
          <Link to="/products/book">A Book</Link>
        </li>
        <li>
          <Link to="/products/pen">A Pen</Link>
        </li>
        <li>
          <Link to="/products/pencil">A Pencil</Link>
        </li>
      </ul>
    </section>
  );
};

export default Products;
